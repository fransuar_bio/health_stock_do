from trytond.pool import Pool
from . import health_stock
from . import shipment

from .wizard import dispense_round_medication

def register():
    Pool.register(
        health_stock.PatientRounding,
        health_stock.PatientRoundingMedicationDispensed,
        health_stock.PatientRoundingEmergency,
        health_stock.PatientRoundingEmergencyMedicament,
        health_stock.PatientRoundingEmergencyMedicalSupply,
        health_stock.Move,
        dispense_round_medication.DispenseRoundMedicationStart,
        dispense_round_medication.DispenseRoundMedicationMedication,
        shipment.ShipmentOut,
        module='health_stock_do', type_='model')
    Pool.register(
        health_stock.CreateInpatientShipment,
        health_stock.CreateStockShipment,
        dispense_round_medication.DispenseRoundMedicationWizard,
        module='health_stock_do', type_='wizard')
