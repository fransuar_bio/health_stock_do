from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateTransition
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.modules.health.core import get_health_professional

from datetime import datetime


class DispenseRoundMedicationStart(ModelView):
    "Dispense Round Medication - Start"
    __name__ = 'gnuhealth.patient.rounding.dispense_medication.start'

    dispense_date = fields.DateTime("Dispense date", required=True)
    medications = \
        fields.One2Many('gnuhealth.patient.rounding.dispense_medication.medication',
                    None, 'Medications', required=True)

    @staticmethod
    def default_dispense_date():
        return datetime.today()


class DispenseRoundMedicationMedication(ModelView):
    "Dispense Round Medication - Medication"
    __name__ = 'gnuhealth.patient.rounding.dispense_medication.medication'

    medicament = \
        fields.Many2One('gnuhealth.medicament', "Medication", required=True)
    product = fields.Many2One('product.product', "Product", required=True)
    #lot = fields.Many2One('stock.lot', "Lot",
            #domain=[('id', 'in', Eval('lot_domain'))])
    #lot_domain = fields.Many2Many('stock.lot', None, None, "Lot domain")
    quantity = fields.Integer("Quantity", required=True,
                domain=[('quantity','<=', Eval('avaible'))])
    avaible = fields.Integer("Avaible", readonly=True)
    shift = fields.Selection([
        (None, ''),
        ('morning', "Morning"),
        ('evening', "Evening"),
        ('night', "Night"),
        ], "Shift", sort=False, required=True)
    healthprof = fields.Many2One('gnuhealth.healthprofessional',
            "Health professional", required=True)
    message = fields.Char("Message", readonly=True)

    @staticmethod
    def default_message():
        return gettext('health_stock_do.msg_dispense_quantity_right_message')

    @fields.depends('quantity', 'avaible')
    def on_change_with_message(self, name=None):
        if self.quantity:
            if self.quantity > self.avaible:
                return gettext('health_stock_do.msg_dispense_quantity_wrong_message')
            return gettext('health_stock_do.msg_dispense_quantity_right_message')
        return ''


class DispenseRoundMedicationWizard(Wizard):
    "Dispense Round Medication - Wizard"
    __name__ = 'gnuhealth.patient.rounding.dispense_medication.wizard'

    start = StateView('gnuhealth.patient.rounding.dispense_medication.start',
                'health_stock_do.dispense_round_medication_start_form', [
                    Button('Cancel', 'end', 'tryton-cancel'),
                    Button('Dispense', 'dispense', 'tryton-ok', default=True)
                    ])

    dispense = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        ShipmentOut = pool.get('stock.shipment.out')
        Round = pool.get('gnuhealth.patient.rounding')
        Medicament = pool.get('gnuhealth.medicament')
        Move = pool.get('stock.move')

        round_id = Transaction().context.get('active_id')

        round_ = Round(round_id)

        rounds = Round.search([
            ('name', '=', round_.name)
            ])

        shipments = ShipmentOut.search([
            ('customer', '=', round_.name.patient.name.id),
            ('state', '=', 'done')
            ])
        moves_origin = Move.search([
                ('origin', 'in', [prescription for prescription in round_.name.medications_order]),
                ('state', '=', 'done')
                ])

        moves = []
        products_qty_shipment = {}
        products_lot_shipment = {}
        #for shipment in shipments:
            #for move in shipment.outgoing_moves:
        for move in moves_origin:
            product_id = move.product.id
            if move.lot:
                lot_id = move.lot.id
                if product_id in products_lot_shipment:
                    products_lot_shipment[product_id].append(lot_id)
                else:
                    products_lot_shipment[product_id] = [lot_id]

            if product_id in products_qty_shipment:
                products_qty_shipment[product_id] += move.quantity
            else:
                products_qty_shipment[product_id] = move.quantity

        for r in rounds:
            for medicament in r.medicaments_dispense:
                product_id = medicament.product.id
                if product_id in products_qty_shipment:
                    products_qty_shipment[product_id] -= medicament.quantity

        res = []
        for product_id in products_qty_shipment:
            medicaments = Medicament.search([
                ('name', '=', product_id)
                ])
            lot = None
            lot_domain = []
            if product_id in products_lot_shipment:
                lot_domain = products_lot_shipment[product_id]
                if len(products_lot_shipment[product_id]) == 1:
                    lot = products_lot_shipment[product_id][0]
                else:
                    lot = None

            if products_qty_shipment[product_id] > 0:
                res.append({
                    'medicament': medicaments[0].id,
                    'product': product_id,
                    #'lot': lot,
                    #'lot_domain': [x.id for x in lot_domain],
                    'quantity': 1,
                    'avaible': int(products_qty_shipment[product_id]) \
                        if products_qty_shipment[product_id] > 0 \
                        else 0,
                    'shift': 'morning',
                    'healthprof': get_health_professional()
                    })

        return {
            'dispense_date': datetime.today(),
            'medications': res,
            }

    def transition_dispense(self):
        pool = Pool()
        Rounding = pool.get('gnuhealth.patient.rounding')
        RoundingMeds = pool.get('gnuhealth.patient.rounding.med_dispensed')

        round_id = Transaction().context['active_id']

        for med in self.start.medications:
            r_med_disp = RoundingMeds()
            r_med_disp.name = round_id
            r_med_disp.medicament = med.medicament.id
            r_med_disp.product = med.product.id
            #r_med_disp.lot = med.lot and med.lot.id
            r_med_disp.quantity = med.quantity
            r_med_disp.shift = med.shift
            r_med_disp.healthprof = med.healthprof.id
            r_med_disp.save()
        round_ = Rounding(round_id)
        round_.dispense_date = self.start.dispense_date
        round_.save()
        return 'end'
