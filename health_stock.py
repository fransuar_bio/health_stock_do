from datetime import datetime

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateTransition
from trytond.pyson import If, Or, Eval, Not, Bool
from trytond.exceptions import UserError
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserWarning
from trytond.modules.health.core import get_health_professional

#__all__ = ['Move', 'PatientRoundingEmergency', 'PatientRoundingEmergencyMedicament',
           #'PatientRoundingEmergencyMedicalSupply']

_STATES = {
    'readonly': Eval('state') == 'done',
    }
_DEPENDS = ['state']


class PatientRounding(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.rounding'

    dispense_date = fields.DateTime("Dispense date", readonly=True)
    medicaments_dispense = \
            fields.One2Many('gnuhealth.patient.rounding.med_dispensed',
                    'name', "Medicaments dispense", readonly=True)

    @classmethod
    @ModelView.button_action('health_stock_do.rounding_dispense_medication_wizard')
    def dispense_medication(cls, roundings):
        pass

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
                'dispense_medication': {},
                })


class PatientRoundingMedicationDispensed(ModelSQL, ModelView):
    "Patient Rounding - Medications Dispensed"
    __name__ = 'gnuhealth.patient.rounding.med_dispensed'

    name = fields.Many2One('gnuhealth.patient.rounding', "Rounding",
                readonly=True)
    medicament = \
        fields.Many2One('gnuhealth.medicament', "Medication",
                readonly=True)
    product = fields.Many2One('product.product', "Product",
                readonly=True)
    #lot = fields.Many2One('stock.lot', "Lot",
            #domain=[('id', 'in', Eval('lot_domain'))])
    #lot_domain = fields.Function(
        #fields.Many2Many('stock.lot', None, None, "Lot domain"),
        #'on_change_with_lot_domain')
    quantity = fields.Integer("Quantity", readonly=True)
    avaible = fields.Integer("Avaible", readonly=True)
    shift = fields.Selection([
        (None, ''),
        ('morning', "Morning"),
        ('evening', "Evening"),
        ('night', "Night"),
        ], "Shift", sort=False, readonly=True)
    healthprof = fields.Many2One('gnuhealth.healthprofessional',
            "Health professional", readonly=True)
    message = fields.Char("Message", readonly=True)

    @staticmethod
    def default_message():
        return gettext('health_stock_do.msg_dispense_quantity_right_message')


class CreateInpatientShipment(Wizard):
    """
    Create Inpatient Shipment
    """
    __name__ = 'health.inpatient.create_shipment'
    start_state = 'generate_shipment'
    generate_shipment = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateInpatientShipment, cls).__setup__()

    def transition_generate_shipment(self):
        pool = Pool()
        Inpatient = pool.get('health.inpatient')
        ids = Transaction().context['active_ids']
        if ids:
            for inpatient in Inpatient.browse(ids):
                if inpatient.state in ('hospitalized', 'confirmed'):
                    inpatient.generate_daily_shipment()
        return 'end'


class CreateStockShipment(Wizard):
    """
    Create Stock Shipment
    """
    __name__ = 'health.stock.create_shipment'
    start_state = 'generate_shipment'
    generate_shipment = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateStockShipment, cls).__setup__()

    def transition_generate_shipment(self):
        ids = Transaction().context['active_ids']
        model = Transaction().context['active_model']
        print('Transaction().context ...', Transaction().context)
        HealthModel = Pool().get(model)
        if ids:
            for record in HealthModel.browse(ids):
                record.generate_shipment()
        return 'end'


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + [
            'health.patient.rounding_emergency',

            ]


class PatientRoundingEmergency(Workflow, ModelSQL, ModelView):
    'Patient Ambulatory Care'
    __name__ = 'health.patient.rounding_emergency'

    hospitalization_location = fields.Many2One(
        'stock.location',
        'Hospitalization Location', domain=[('type', '=', 'storage')],
        states={
            'required': If(Or(
                Bool(Eval('medicaments')),
                Bool(Eval('medical_supplies'))), True, False),
            'readonly': Eval('state') == 'done',
        }, depends=_DEPENDS)
    medicaments = fields.One2Many(
        'health.patient.rounding_emergency.medicament', 'emergency', 'Medicaments',
        states=_STATES, depends=_DEPENDS)
    medical_supplies = fields.One2Many(
        'health.patient.rounding_emergency.medical_supply',
        'emergency', 'Medical Supplies',
        states=_STATES, depends=_DEPENDS)
    moves = fields.One2Many(
        'stock.move', 'origin', 'Stock Moves',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super(PatientRoundingEmergency, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
        ))
        cls._buttons.update({
            'done': {
                'invisible': ~Eval('state').in_(['draft']),
            }})

    @classmethod
    def copy(cls, roundings, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        return super(PatientRoundingEmergency, cls).copy(roundings, default=default)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, roundings):
        signing_hp = get_health_professional()

        lines_to_ship = {}
        medicaments_to_ship = []
        supplies_to_ship = []

        for rounding in roundings:
            for medicament in rounding.medicaments:
                medicaments_to_ship.append(medicament)

            for medical_supply in rounding.medical_supplies:
                supplies_to_ship.append(medical_supply)

        lines_to_ship['medicaments'] = medicaments_to_ship
        lines_to_ship['supplies'] = supplies_to_ship

        cls.create_stock_moves(roundings, lines_to_ship)

        cls.write(roundings, {
            'signed_by': signing_hp,
            'evaluation_end': datetime.now()
            })

    @classmethod
    def create_stock_moves(cls, roundings, lines):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        moves = []
        for rounding in roundings:
            for medicament in lines['medicaments']:
                move_info = {}
                move_info['origin'] = str(rounding)
                move_info['product'] = medicament.medicament.name.id
                move_info['uom'] = medicament.medicament.name.default_uom.id
                move_info['quantity'] = medicament.quantity
                move_info['from_location'] = \
                    rounding.hospitalization_location.id
                move_info['to_location'] = \
                    rounding.emergency.patient.name.customer_location.id
                move_info['unit_price'] = medicament.medicament.name.list_price
                move_info['cost_price'] = medicament.medicament.name.cost_price #Added to core
                if medicament.lot:
                    if medicament.lot.expiration_date \
                            and medicament.lot.expiration_date < Date.today():
                        raise UserError('Expired medicaments')
                    move_info['lot'] = medicament.lot.id
                moves.append(move_info)
            for medical_supply in lines['supplies']:
                move_info = {}
                move_info['origin'] = str(rounding)
                move_info['product'] = medical_supply.product.id
                move_info['uom'] = medical_supply.product.default_uom.id
                move_info['quantity'] = medical_supply.quantity
                move_info['from_location'] = \
                    rounding.hospitalization_location.id
                move_info['to_location'] = \
                    rounding.emergency.patient.name.customer_location.id
                move_info['unit_price'] = medical_supply.product.list_price
                if medical_supply.lot:
                    if medical_supply.lot.expiration_date \
                            and medical_supply.lot.expiration_date < \
                            Date.today():
                        raise UserError('Expired supplies')
                    move_info['lot'] = medical_supply.lot.id
                moves.append(move_info)
        new_moves = Move.create(moves)
        Move.write(new_moves, {
            'state': 'done',
            'effective_date': Date.today(),
            })

        return True


class PatientRoundingEmergencyMedicament(ModelSQL, ModelView):
    'Patient Rounding Medicament'
    __name__ = 'health.patient.rounding_emergency.medicament'

    emergency = fields.Many2One('health.patient.rounding_emergency', 'Ambulatory ID')
    medicament = fields.Many2One(
        'gnuhealth.medicament', 'Medicament',
        required=True)
    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char(
        'Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One(
        'stock.lot', 'Lot', depends=['product'],
        domain=[('product', '=', Eval('product'))])

    @staticmethod
    def default_quantity():
        return 1

    @fields.depends('medicament')
    def on_change_medicament(self):
        if self.medicament:
            self.product = self.medicament.name.id

        else:
            self.product = None


class PatientRoundingEmergencyMedicalSupply(ModelSQL, ModelView):
    'Patient Rounding Medical Supply'
    __name__ = 'health.patient.rounding_emergency.medical_supply'

    emergency = fields.Many2One('health.patient.rounding_emergency', 'Ambulatory ID')
    product = fields.Many2One(
        'product.product', 'Medical Supply',
        domain=[('is_medical_supply', '=', True)], required=True)
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char(
        'Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One(
        'stock.lot', 'Lot', depends=['product'],
        domain=[
            ('product', '=', Eval('product')),
            ])

    @staticmethod
    def default_quantity():
        return 1
