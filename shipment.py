from trytond.model import fields, ModelView, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Or
from datetime import datetime, timedelta


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    prescription = fields.Many2One('gnuhealth.prescription.order', "Prescription",
                domain=[
                    ('patient.name', '=', Eval('customer')),
                    ('state', '=', 'done'),
                    ('prescription_date', '>', Eval('yesterday'))
                    ],
                states = {'readonly': Or(~Eval('warehouse'), Eval('state') != 'draft')}
                )
    yesterday = fields.DateTime("Yesterday")

    @fields.depends('customer')
    def on_change_with_yesterday(self, name=None):
        yesterday = datetime.today() - timedelta(days=1)
        return yesterday

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('prescription_unique', Unique(t, t.prescription),
                'This prescription is already related to another shipment'),
            ]
